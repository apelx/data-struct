#include <stdio.h>
#include <stdlib.h>

#define MAX_SIZE 100

// 定义邻接表节点
typedef struct node {
	int index;
	struct node *next;
} Node;

// 定义邻接表条目
typedef struct {
	// 顶点名
	char vertext;
	Node *link;
} AdjacencyTableEntry;


// 定义邻接表
typedef struct {
	AdjacencyTableEntry *entries[MAX_SIZE];
	// 顶点数量
	int vertextCount;
	// 边数量
	int arcCount;
} AdjacencyTable;


/*
	邻接表创建无向图

    v0 ———— v2
	|	      /	| \
	|	    /	|	\
	|	  /		|	  v4
	|   /		|	/
	| /			| /
	v1 ———— v3

  邻接表,链表存的是下标
下标
0		v0 []-> 1 -> 2
1		v1 []-> 0 -> 2 -> 3
2		v2 []-> 0 -> 1 -> 3 ->4
3		v3 []-> 1 -> 2 -> 4
4		v4 []-> 2 -> 3
 */
int findEntryInTableByVertext(AdjacencyTable*, char);
Node* findAdjacencyTableEntryNodeTail(AdjacencyTableEntry *);
AdjacencyTable *buildAdjacencyTable() {
	AdjacencyTable *table = malloc(sizeof(AdjacencyTable));
	int vertextCount,arcCount;
	int i,im,in;
	char m,n;
	Node *p,*tempNode;

	printf("please input vertext count: \n");
	scanf("%d", &vertextCount);
	printf("please input arc count: \n");
	scanf("%d", &arcCount);
	table->vertextCount = vertextCount;
	table->arcCount = arcCount;

	for (i = 0; i < vertextCount; i++) {
		table->entries[i] = malloc(sizeof(AdjacencyTableEntry));
		printf("please input number of [%d] vertext: \n", i+1);
		scanf(" %c", &(table->entries[i])->vertext);
		table->entries[i]->link = NULL;
	}

	
	for(i = 0; i < arcCount; i++) {
		printf("please input number of [%d] arc. eg. a b: \n", i+1);
		scanf(" %c %c", &m, &n);
		printf("input m: %c, n: %c\n", m, n);
		im = findEntryInTableByVertext(table, m);
		if(im == -1) {
			printf("vertext %c not found!\n", m);
			exit(-1);
		}
		in = findEntryInTableByVertext(table, n);
		if(in == -1) {
			printf("vertext %c not found!\n", n);
			exit(-1);
		}
		// 使用尾插! m
		p = malloc(sizeof(Node));
		p->index = in;
		p->next = NULL;
		tempNode = findAdjacencyTableEntryNodeTail(table->entries[im]);
		if(tempNode == NULL) {
			table->entries[im]->link = p;
		}else {
			tempNode->next = p;			
		}

		// 使用尾插! n
		p = malloc(sizeof(Node));
		p->index = im;
		p->next = NULL;
		tempNode = findAdjacencyTableEntryNodeTail(table->entries[in]);
		if(tempNode == NULL) {
			table->entries[in]->link = p;
		}else {
			tempNode->next = p;			
		}
	}
	return table;
}

// 在邻接表数组中找到指定顶点的entry
int findEntryInTableByVertext(AdjacencyTable *table, char vertext) {
	int i;
	for(i = 0; i < table->vertextCount; i++) {
		if(table->entries[i]->vertext == vertext) {
			return i;
		}	
	}
	return -1;
}

// 查找邻接表条目中节点链表最后一个
Node* findAdjacencyTableEntryNodeTail(AdjacencyTableEntry *entry) {
	Node* node = entry->link;;
	while(node != NULL) {
		if(node->next == NULL) {
			break;
		}
		node = node->next;
	}
	return node;
	
}

void printfAdjacencyTable(AdjacencyTable *table) {
	int i;
	Node *node;
	for(i = 0; i < table->vertextCount; i++) {
		printf("vertext: %c, arc node: \n", table->entries[i]->vertext);
		node = table->entries[i]->link;
		if(node == NULL) {
			printf("empty!\n");
		}else {
			while(node != NULL) {
				printf("%d ", node->index);
				node = node->next;
			}
			printf("\n");
		}
	}
}
int main37() {
	/*
		输入顺序:
		5
		7
		0
		1
		2
		3
		4
		0 1
		0 2
		1 2
		1 3
		2 3
		2 4
		3 4
	*/
	
	AdjacencyTable *table = buildAdjacencyTable();
	printfAdjacencyTable(table);
	
	return 0;
}
