#include <stdio.h>
#include <stdlib.h>
#include "排序元素.h"

/*
	直接选择排序

  每一次在无序列表中选择一个最小的放到列表最前面
*/
void directChooseSort(SortList list, int n) {
	int i, j, minIndex;
	for(i = 1; i < n; i++) {
		minIndex = i;
		for(j = i+1; j <= n; j++) {
			if(list[j].key < list[minIndex].key) {
				minIndex = j;
			}
		}
		if(minIndex != i) {
			list[0] = list[i];
			list[i] = list[minIndex];
			list[minIndex] = list[0];
		}
	}
}

int main45() {
	int n = 8;
	SortList list = {
		{0}, {46}, {39}, {17}, {23}, {28}, {55}, {46}, {18}
	};
	printSortList(list, 1, n);

	directChooseSort(list, n);

	printSortList(list, 1, n);
	return 0;
	return 0;
}