#include <stdio.h>
#include <stdlib.h>

typedef char DataType;

// 定义队列节点
typedef struct QueueNode {
	DataType data;
	struct QueueNode *next;
}QueueNode;

// 定义队列
typedef struct queue{
	struct QueueNode *head, *rear;
	int size;
} LinkedQueue;


// 初始化queue 
LinkedQueue* initLinkedQueue_();

// 判断队列是否为空
int emptyLinkedQueue_(LinkedQueue *);
// 入队
void offerLinkedQueue_(LinkedQueue *, DataType );

// 出队
DataType popLinkedQueue_(LinkedQueue *);


void printLinkedQueue_(LinkedQueue *);
