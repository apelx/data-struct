#include <stdio.h>
#include <stdlib.h>

#define STACK_SIZE 100
typedef char DataType;

// ����ջ
typedef struct {
	DataType data[STACK_SIZE];
	int top;
} Stack;

// ----------------------------------ջ����-------------------------------------------
Stack *initStack() {
	Stack *stack = malloc(sizeof(Stack));
	stack->top = -1;
	return stack;
}

int emptyStack(Stack *stack) {
	return stack->top == -1;
}

int fullStack(Stack *stack) {
	return stack->top  == STACK_SIZE - 1;
}

void pushStack(Stack *stack, DataType data) {
	if(fullStack(stack)) {
		printf("push stack full.");
		exit(-1);
	}
	stack->data[++stack->top] = data;
}

DataType popStack(Stack *stack) {
	if(emptyStack(stack)) {
		printf("pop empty stack.");
		exit(-1);
	}
	return stack->data[stack->top--];
}

DataType getTopStack(Stack *stack) {
	if(emptyStack(stack)) {
		printf("pop empty stack.");
		exit(-1);
	}
	return stack->data[stack->top];
}


void printStack(Stack *stack) {
	int i;
	if(emptyStack(stack)) {
		printf("print empty stack.\n");
		return;
	}
	for(i = 0; i <= stack->top; i++) {
		printf("%c ", stack->data[i]);
	}
	printf("\n");
}