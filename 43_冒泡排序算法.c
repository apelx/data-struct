#include <stdio.h>
#include <stdlib.h>
#include "排序元素.h"







/**
	冒泡排序

  从后往前，i 与 i-1 比较 后面的小于前面的则交换，小的往前，i--; 每一次排序都排出一个最小值放到最前面
  借助数组0下标作为交换临时空间
  
	
         46  39  17  23  28  55  18  46
下标  0  1   2   3   4   5   6   7   8
*/

void effervescenceSort(SortList list, int n) {
	int i,j,flag;
	// 比较n-1次
	for(i = 1; i < n; i++) {
		flag = 0;
		for(j = n; j >= i - 1; j--) {
			if(list[j].key < list[j-1].key) {
				list[0] = list[j] ;
				list[j] = list[j-1];
				list[j-1] = list[0];
				flag = 1;
			}
		}
		if(!flag) return;
	}
}

/**
	反向冒泡排序 - 大的往后排序
*/
void effervescenceRevSort(SortList list, int n) { 
	int i, j, flag;
	for (i = n; i >= 2; i--) {
		flag = 0;
		for(j = 1; j <= i - 1; j++) {
			if(list[j].key > list[j+1].key) {
				list[0] = list[j];
				list[j] = list[j+1];
				list[j+1] = list[0];
				flag = 1;
			}
		}
		if(!flag) return;
	}
}


/*
	双向冒泡排序

  双向冒泡排序则是交替改变扫描方向，即一趟从下而上通过两个相邻关键字的比较，
  将关键字最小的记录换至最上面的位置，再一趟则是从第二个记录开始向下通过两个相邻记录关键字的比较，
  将关键字最大的记录换至最下面的位置；然后再从倒数第二个记录开始向上两两比较至顺数第二个记录，
  将其中关键字较小的记录换至第二个记录位置，再从第三个记录向下至倒数第二个记录两两比较，
  将其中较大关键字的记录换至倒数第二个位置，以此类推，直到全部有序为止

*/
void bidirectionalEffervescenceSort(SortList list, int n) {
	int index = 1, i;
	int flag = 1;
	while (flag) {
		flag = 0;
		for(i = n + 1 - index ; i >= index + 1; i--) {
			if(list[i].key < list[i-1].key) {
				list[0] = list[i];
				list[i] = list[i-1];
				list[i-1] = list[0];
				flag = 1;
			}
		}
	
		for(i = index + 1; i <= n - index; i++) {
			if(list[i].key > list[i+1].key) {
				list[0] = list[i];
				list[i] = list[i+1];
				list[i+1] = list[0];
				flag = 1;
			}
		}
		index++;
	}
}




void maopao(SortList sortList, int n) {
	int i,j,flag;
	if(n <= 1) {
		return;
	}
	for(i = 1; i < n; i++) {
		flag = 0;
		for(j = n; j > i; j--) {
			if(sortList[j].key < sortList[j-1].key) {
				sortList[0] = sortList[j];
				sortList[j] = sortList[j-1];
				sortList[j-1] = sortList[0];
				flag = 1;
			}
		}
		if(!flag) {return;}
	}
}

int main43() {

	int n = 8;
	SortList s1 = {
		{0}, {46}, {39}, {17}, {23}, {28}, {55}, {46}, {18}
	};
	SortList s2 = {
		{0}, {46}, {39}, {17}, {23}, {28}, {55}, {46}, {18}
	};
	SortList s3 = {
		{0}, {46}, {39}, {17}, {23}, {28}, {55}, {46}, {18}
	};

	printSortList(s1, 1, n);


	/*
	effervescenceSort(s1, n);
	printSortList(s1, 1, n);



	effervescenceRevSort(s2, n);
	printSortList(s2, 1, n);


	bidirectionalEffervescenceSort(s3, n);
	printSortList(s3, 1, n);
	*/
	maopao(s1, n);
	printSortList(s1, 1, n);
	return 0;
}