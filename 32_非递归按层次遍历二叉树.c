#include <stdio.h>
#include <stdlib.h>
#include "二叉树.h"

/**
  非递归按层次遍历二叉树
            A          ...第1层
         /     \       
		B       C      ...第2层
      /       /   \
	 D	     E     F   ...第3层
	
 遍历结果: ABCDEF


// 初始化队列
BTreeNodeQueue *initBTreeNodeQueue();
// 判断队列是否为空
int emptyBTreeNodeQueue(BTreeNodeQueue*);
// 判断队列是否满
int fullBTreeNodeQueue(BTreeNodeQueue*);
// 入队  1成功 0失败
int enterBTreeNodeQueue(BTreeNodeQueue*, BTreeNode*);
// 出队 非空弹出队首元素,否则返回NULL
BTreeNode popBTreeNodeQueue(BTreeNodeQueue*);
// 获取队首元素  非空返回队首元素,否则返回NULL
BTreeNode* topBTreeNodeQueue(BTreeNodeQueue*);
// 队列长度
int sizeofBTreeNodeQueue(BTreeNodeQueue *);
// 打印队列
void printBTreeNodeQueue(BTreeNodeQueue*);

*/
void levelForeachBTreeNoRecursion(BTree btree) {	
	BTreeNodeQueue *queue = initBTreeNodeQueue();
	BTreeNode *node = btree;
	// 根节点入队
	enterBTreeNodeQueue(queue, node);
	while(!emptyBTreeNodeQueue(queue)) {
		node = popBTreeNodeQueue(queue);
		printf("%c ", node->data);
		if(node->lchild != NULL) {
			enterBTreeNodeQueue(queue, node->lchild);
		}
		if(node->rchild != NULL) {
			enterBTreeNodeQueue(queue, node->rchild);
		}
	}
	free(queue);
	printf("\n");
}

int main32() {


	/* 队列测试
	BTreeNodeQueue *queue = initBTreeNodeQueue();
	BTreeNode *n1 = malloc(sizeof(BTreeNode));
	BTreeNode *n2 = malloc(sizeof(BTreeNode));
	BTreeNode *n3 = malloc(sizeof(BTreeNode));
	BTreeNode *n4 = malloc(sizeof(BTreeNode));
	n1->data = 'A';
	n2->data = 'B';
	n3->data = 'C';
	n4->data = 'D';
	enterBTreeNodeQueue(queue, n1);
	enterBTreeNodeQueue(queue, n2);
	enterBTreeNodeQueue(queue, n3);
	printBTreeNodeQueue(queue);
	printf("queue size %d\n", sizeofBTreeNodeQueue(queue));

	printf("pop: %c \n", popBTreeNodeQueue(queue)->data);

	printBTreeNodeQueue(queue);
	printf("queue size %d\n", sizeofBTreeNodeQueue(queue));

	enterBTreeNodeQueue(queue, n4);
	printBTreeNodeQueue(queue);
	printf("queue size %d\n", sizeofBTreeNodeQueue(queue));
	*/

	/**
	  对应二叉树为
                A① 
             /     \
		    B②     C③
              \       \
			   E⑤     G⑦
              /
			 J⑩
	*/
	

	// [A] [B] [C] [@] [E] [@] [G] [@] [@] [J]            数组0任意填充
	DataType arr[] = {'@','A', 'B', 'C', '@', 'E', '@', 'G', '@', '@', 'J', '\0'};
	BTree btree = initBTreeByCompleteBTreeArr(arr);
	levelForeachBTreeNoRecursion(btree);
	return 0;
}