#include <stdio.h>
#include <stdlib.h>

typedef char DataType;

typedef struct QueueNode {
	DataType data;
	struct QueueNode *next;
}QueueNode;

// 定义循环队列
typedef struct {
	QueueNode *rear;
	int size;
} LinkedCirculateQueue;


void offerLinkedCirculateQueue(LinkedCirculateQueue *queue, DataType);

// 初始化循环队列
LinkedCirculateQueue *initLinkedCirculateQueue() {
	QueueNode *head = malloc(sizeof(QueueNode));
	LinkedCirculateQueue *queue = malloc(sizeof(LinkedCirculateQueue));
	queue->rear = head;
	queue->rear->next = queue->rear;
	queue->size = 0;
	return queue;
}

// 初始化循环队列使用数组
LinkedCirculateQueue *initLinkedCirculateQueueWithArr(DataType arr[]) {
	LinkedCirculateQueue *queue = initLinkedCirculateQueue();
	int i = 0;
	while(arr[i] != '\0') {
		offerLinkedCirculateQueue(queue, arr[i]);
		i++;
	}
	queue->size = i;
	return queue;
}

// 判空
int emptyLinkedCirculateQueue(LinkedCirculateQueue *queue) {
	return queue->rear->next == queue->rear;
}

// 入队
void offerLinkedCirculateQueue(LinkedCirculateQueue *queue, DataType data) {
	QueueNode *node = malloc(sizeof(QueueNode));
	node->data = data;
	node->next = queue->rear->next;
	queue->rear->next = node;
	queue->rear = node;
	queue->size++;
}

// 出队
// 删除头节点，让队头节点做头节点
DataType popLinkedCirculateQueue(LinkedCirculateQueue *queue) {
	QueueNode *head;
	if(emptyLinkedCirculateQueue(queue)) {
		printf("pop with empty queue! ");
		exit(-1);
	}
	head = queue->rear->next;
	queue->rear->next = head->next;
	free(head);
	queue->size--;
	return queue->rear->next->data;
}


void printLinkedCirculateQueue(LinkedCirculateQueue *queue) {
	QueueNode *p;
	if(emptyLinkedCirculateQueue(queue)) {
		printf("print with empty queue! ");
		return;
	}
	p = queue->rear->next->next;
	while(p != queue->rear) {
		printf("%c ", p->data);
		p = p->next;
	}
	// print rear
	printf("%c ", p->data);
	printf(" -----> size: %d\n", queue->size);
	
}

int main21() {

	DataType arr[] = {'a', 'b', 'c', 'd', 'e', '\0'};
	int i = 0;
	LinkedCirculateQueue *queue = initLinkedCirculateQueueWithArr(arr);
	printLinkedCirculateQueue(queue);

	for(i = 0; i < 2; i++) {
		printf("pop: %c \n", popLinkedCirculateQueue(queue));
		printLinkedCirculateQueue(queue);
	}


	printf("offer: 'f' \n");
	offerLinkedCirculateQueue(queue, 'f');
	printLinkedCirculateQueue(queue);

	printf("offer: 'g' \n");
	offerLinkedCirculateQueue(queue, 'g');
	printLinkedCirculateQueue(queue);


	for(i = 0; i < 5; i++) {
		printf("pop: %c \n", popLinkedCirculateQueue(queue));
		printLinkedCirculateQueue(queue);
	}

	return 0;
}