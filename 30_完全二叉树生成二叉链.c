#include <stdio.h>
#include <stdlib.h>
#include "二叉树.h"


// 输入完全二叉树的数组表现形式 [A] [B] [C] [@] [E] [@] [G] [@] [@] [J]
// 转换成 二叉链表
BTree initBTreeByInputCompleteBTree() { 
	BTree root = NULL;
	BTreeNode *nodes[100];
	int i = 1;
	char temp;
	while((temp = getchar()) != '\n') {
		if(temp != '@') {
			nodes[i] = malloc(sizeof(BTreeNode));
			nodes[i]->data = temp;
			nodes[i]->lchild = nodes[i]->rchild = NULL;
			if(root == NULL) {
				root = nodes[i];
			} else {
				// left child
				if(i % 2 == 0) { 
					nodes[i/2]->lchild = nodes[i];
				} 
				// right child
				else {
					nodes[i/2]->rchild = nodes[i];
				}
			}
		}
		i++;
	}
	return root;
}


int main30() {
	/*
	// [A] [B] [C] [@] [E] [@] [G] [@] [@] [J]            数组0任意填充
	DataType arr[] = {'@','A', 'B', 'C', '@', 'E', '@', 'G', '@', '@', 'J', '\0'};
	BTree btree = initBTreeByCompleteBTreeArr(arr);

	// A B E J C G
	precedenceForeachBTree(btree);
	printf("\n");
	// B J E A C G  
	middleForeachBTree(btree);
	printf("\n");
	// J E B G C A
	postForeachBTree(btree);
	printf("\n");
	*/
	
	
	// 输入 [A] [B] [C] [@] [E] [@] [G] [@] [@] [J]
	BTree btree = initBTreeByInputCompleteBTree();
	// A B E J C G
	precedenceForeachBTree(btree);
	printf("\n");
	// B J E A C G  
	middleForeachBTree(btree);
	printf("\n");
	// J E B G C A
	postForeachBTree(btree);
	printf("\n");

	return 0;
}