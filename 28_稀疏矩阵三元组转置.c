#include <stdio.h>
#include <stdlib.h>
#include "稀疏矩阵三元组.h"

/**
 * 试写一个算法，实现以三元组表结构存储的稀疏矩阵的转置（行列对换）
 *
 * 矩阵M
 * 0  3  0  5  0
 * 0  0  -2 0  0
 * 1  0  0  0  6
 * 0  0  8  0  0
 *
 * M三元组
 * 0  1  3
   0  3  5
   1  2  -2
   2  0  1
   2  4  6
   3  2  8
 *
 * 转置后矩阵T
 * 0  0  1  0
 * 3  0  0  0 
 * 0 -2  0  8
 * 5  0  0  0
 * 0  0  6  0

   T三元组
   0  2  1
   1  0  3
   2  1  -2
   2  3  8
   3  0  5 
   4  2  6

  * 思路：
  遍历原矩阵三元组，按列从小到大排序，然后对调三元组中的行列
  
 */
SparseMatrixTriTuple* trsmatSparseMatrixTriTuple(SparseMatrixTriTuple origin) {
	int i,col,index = 0;
	SparseMatrixTriTuple* smtt = malloc(sizeof(SparseMatrixTriTuple));
	smtt->column_count = origin.line_count;
	smtt->line_count = origin.column_count;
	smtt->no_zero_count = origin.no_zero_count;
	if(origin.no_zero_count > 0) {
		// 遍历原矩阵的列数
		for (col = 0; col < origin.column_count; col++) {
			// 遍历原矩阵三元组(有多少个非0元组就有多少行)
			for(i = 0; i < origin.no_zero_count; i++) {
				if(origin.lines[i].column == col) {
					smtt->lines[index].line = origin.lines[i].column;
					smtt->lines[index].column = origin.lines[i].line;
					smtt->lines[index].data = origin.lines[i].data;
					index++;
				}
			}
		}
	}
	return smtt;
}

// 创建稀疏矩阵的三元组
SparseMatrixTriTuple* createSparseMatrixTriTuple1(int sparseMatrix[4][5], int line_count, int column_count){
	SparseMatrixTriTuple *smtt = malloc(sizeof(SparseMatrixTriTuple));
	int i,j,index = 0;

	for (i = 0; i < line_count; i++){
		for (j = 0; j < column_count; j++){
			if(sparseMatrix[i][j] != 0) {
				smtt->lines[index].line = i;
				smtt->lines[index].column = j;
				smtt->lines[index].data = sparseMatrix[i][j];
				index++;
			}
		}
	}
	smtt->line_count = line_count;
	smtt->column_count = column_count;
	smtt->no_zero_count = index;
	return smtt;
}

int main28() {
	int i;
	int sparseMatrix[4][5] = {
		{0, 3, 0,  5, 0},
		{0, 0, -2, 0, 0},
		{1, 0, 0,  0, 6},
		{0, 0, 8,  0, 0}
	};
	SparseMatrixTriTuple *smtt1;
	SparseMatrixTriTuple *smtt = createSparseMatrixTriTuple1(sparseMatrix, 4, 5);
	printf("line_count: %d, column_count: %d, no_zero_count: %d\n", smtt->line_count, smtt->column_count, smtt->no_zero_count);

	for(i = 0; i < smtt->no_zero_count; i++) {
		printf("%d  %d  %d\n", smtt->lines[i].line, smtt->lines[i].column, smtt->lines[i].data);
	}

	smtt1 = trsmatSparseMatrixTriTuple(*smtt);

	printf("\n-------- after convert \n");
	for(i = 0; i < smtt1->no_zero_count; i++) {
		printf("%d  %d  %d\n", smtt1->lines[i].line, smtt1->lines[i].column, smtt1->lines[i].data);
	}
	return 0;
}