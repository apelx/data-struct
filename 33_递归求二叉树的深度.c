#include <stdio.h>
#include <stdlib.h>
#include "二叉树.h"

/**
	递归求二叉树的深度
 */
int bTreeDepth(BTreeNode *root) {
	int l_depth = 0, r_depth = 0;
	if(root == NULL) {
		return 0;
	}
	l_depth = bTreeDepth(root->lchild);
	r_depth = bTreeDepth(root->rchild);
	return 1 + (l_depth > r_depth ? l_depth : r_depth);
}

int main33() {
	/**
	  对应二叉树为
                A① 
             /     \
		    B②     C③
              \       \
			   E⑤     G⑦
              /
			 J⑩
	*/
		// [A] [B] [C] [@] [E] [@] [G] [@] [@] [J]            数组0任意填充
	DataType arr[] = {'@','A', 'B', 'C', '@', 'E', '@', 'G', '@', '@', 'J','\0'};
	BTree btree = initBTreeByCompleteBTreeArr(arr);

	printf("depth: %d\n", bTreeDepth(btree));

	return 0;
}