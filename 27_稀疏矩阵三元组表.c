#include <stdio.h>
#include <stdlib.h>
#include "稀疏矩阵三元组.h"

/**
 * 试写一个算法，建立稀疏矩阵的三元组表
 */
/*

// 三元组数据行
typedef struct {
	int line, column;
	DataType data;
} TriTupleLine;

// 稀疏矩阵三元组
typedef struct {
	// 三元组中所有行
	TriTupleLine lines[LINE_SIZE];
	// 稀疏矩阵的行数
	int line_count;
	// 稀疏矩阵的列数
	int column_count;
	// 非零元素的个数
	int no_zero_count;
} SparseMatrixTriTuple;

  */

/**
 * 创建稀疏矩阵三元组
 *
 * @param sparseMatrix 稀疏矩阵
 * @param m 稀疏矩阵行数
 * @param n 稀疏矩阵列数
 */

SparseMatrixTriTuple* createSparseMatrixTriTuple(int sparseMatrix[][4], int line_count, int column_count){
	SparseMatrixTriTuple *smtt = malloc(sizeof(SparseMatrixTriTuple));
	int i,j,index = 0;

	for (i = 0; i < line_count; i++){
		for (j = 0; j < column_count; j++){
			if(sparseMatrix[i][j] != 0) {
				smtt->lines[index].line = i;
				smtt->lines[index].column = j;
				smtt->lines[index].data = sparseMatrix[i][j];
				index++;
			}
		}
	}
	smtt->line_count = line_count;
	smtt->column_count = column_count;
	smtt->no_zero_count = index;
	return smtt;
}


int main27() {
	int i;
	int sparseMatrix[][4] = {
		{0,  0, 1, 0},
		{0,  5, 0, 0},
		{0,  0, 0, 0},
		{-4, 0, 0, -7}
	};

	SparseMatrixTriTuple *smtt = createSparseMatrixTriTuple(sparseMatrix, 4, 4);
	printf("line_count: %d, column_count: %d, no_zero_count: %d\n", smtt->line_count, smtt->column_count, smtt->no_zero_count);

	for(i = 0; i < smtt->no_zero_count; i++) {
		printf("line: %d, column: %d, data: %d\n", smtt->lines[i].line, smtt->lines[i].column, smtt->lines[i].data);
	}
	/**
	* result:
	* 
	*  0  2  1
	*  1  1  5
	*  3  0  -4
	*  3  3  -7
	*/
	return 0;
}