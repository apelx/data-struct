# Microsoft Developer Studio Project File - Name="DATA_STRUCTURE" - Package Owner=<4>
# Microsoft Developer Studio Generated Build File, Format Version 6.00
# ** DO NOT EDIT **

# TARGTYPE "Win32 (x86) Console Application" 0x0103

CFG=DATA_STRUCTURE - Win32 Debug
!MESSAGE This is not a valid makefile. To build this project using NMAKE,
!MESSAGE use the Export Makefile command and run
!MESSAGE 
!MESSAGE NMAKE /f "DATA_STRUCTURE.mak".
!MESSAGE 
!MESSAGE You can specify a configuration when running NMAKE
!MESSAGE by defining the macro CFG on the command line. For example:
!MESSAGE 
!MESSAGE NMAKE /f "DATA_STRUCTURE.mak" CFG="DATA_STRUCTURE - Win32 Debug"
!MESSAGE 
!MESSAGE Possible choices for configuration are:
!MESSAGE 
!MESSAGE "DATA_STRUCTURE - Win32 Release" (based on "Win32 (x86) Console Application")
!MESSAGE "DATA_STRUCTURE - Win32 Debug" (based on "Win32 (x86) Console Application")
!MESSAGE 

# Begin Project
# PROP AllowPerConfigDependencies 0
# PROP Scc_ProjName ""
# PROP Scc_LocalPath ""
CPP=cl.exe
RSC=rc.exe

!IF  "$(CFG)" == "DATA_STRUCTURE - Win32 Release"

# PROP BASE Use_MFC 0
# PROP BASE Use_Debug_Libraries 0
# PROP BASE Output_Dir "Release"
# PROP BASE Intermediate_Dir "Release"
# PROP BASE Target_Dir ""
# PROP Use_MFC 0
# PROP Use_Debug_Libraries 0
# PROP Output_Dir "Release"
# PROP Intermediate_Dir "Release"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
# ADD BASE CPP /nologo /W3 /GX /O2 /D "WIN32" /D "NDEBUG" /D "_CONSOLE" /D "_MBCS" /YX /FD /c
# ADD CPP /nologo /W3 /GX /O2 /D "WIN32" /D "NDEBUG" /D "_CONSOLE" /D "_MBCS" /YX /FD /c
# ADD BASE RSC /l 0x804 /d "NDEBUG"
# ADD RSC /l 0x804 /d "NDEBUG"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib /nologo /subsystem:console /machine:I386
# ADD LINK32 kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib /nologo /subsystem:console /machine:I386

!ELSEIF  "$(CFG)" == "DATA_STRUCTURE - Win32 Debug"

# PROP BASE Use_MFC 0
# PROP BASE Use_Debug_Libraries 1
# PROP BASE Output_Dir "Debug"
# PROP BASE Intermediate_Dir "Debug"
# PROP BASE Target_Dir ""
# PROP Use_MFC 0
# PROP Use_Debug_Libraries 1
# PROP Output_Dir "Debug"
# PROP Intermediate_Dir "Debug"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
# ADD BASE CPP /nologo /W3 /Gm /GX /ZI /Od /D "WIN32" /D "_DEBUG" /D "_CONSOLE" /D "_MBCS" /YX /FD /GZ /c
# ADD CPP /nologo /W3 /Gm /GX /ZI /Od /D "WIN32" /D "_DEBUG" /D "_CONSOLE" /D "_MBCS" /YX /FD /GZ /c
# ADD BASE RSC /l 0x804 /d "_DEBUG"
# ADD RSC /l 0x804 /d "_DEBUG"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib /nologo /subsystem:console /debug /machine:I386 /pdbtype:sept
# ADD LINK32 kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib /nologo /subsystem:console /debug /machine:I386 /pdbtype:sept

!ENDIF 

# Begin Target

# Name "DATA_STRUCTURE - Win32 Release"
# Name "DATA_STRUCTURE - Win32 Debug"
# Begin Group "Source Files"

# PROP Default_Filter "cpp;c;cxx;rc;def;r;odl;idl;hpj;bat"
# Begin Source File

SOURCE=".\20_链式存储队列.c"
# End Source File
# Begin Source File

SOURCE=".\21_链式存储循环队列.c"
# End Source File
# Begin Source File

SOURCE=".\22_应用_后缀表达式.c"
# End Source File
# Begin Source File

SOURCE=".\23_矩阵转置.c"
# End Source File
# Begin Source File

SOURCE=".\24_例题_马鞍点.c"
# End Source File
# Begin Source File

SOURCE=".\25_对称矩阵的压缩存储.c"
# End Source File
# Begin Source File

SOURCE=".\26_三角矩阵.c"
# End Source File
# Begin Source File

SOURCE=".\27_稀疏矩阵三元组表.c"
# End Source File
# Begin Source File

SOURCE=".\28_稀疏矩阵三元组转置.c"
# End Source File
# Begin Source File

SOURCE=".\29_广义表构建二叉树.c"
# End Source File
# Begin Source File

SOURCE=".\30_完全二叉树生成二叉链.c"
# End Source File
# Begin Source File

SOURCE=".\31_二叉树的非递归遍历.c"
# End Source File
# Begin Source File

SOURCE=".\32_非递归按层次遍历二叉树.c"
# End Source File
# Begin Source File

SOURCE=".\33_递归求二叉树的深度.c"
# End Source File
# Begin Source File

SOURCE=".\34_递归在二叉树中查找值.c"
# End Source File
# Begin Source File

SOURCE=".\35_递归查找二叉树中指定节点所在的层.c"
# End Source File
# Begin Source File

SOURCE=".\36_邻接矩阵创建图.c"
# End Source File
# Begin Source File

SOURCE=".\37_邻接表创建图.c"
# End Source File
# Begin Source File

SOURCE=".\38_深度优先遍历邻接矩阵图.c"
# End Source File
# Begin Source File

SOURCE=".\39_深度优先遍历邻接表图.c"
# End Source File
# Begin Source File

SOURCE=".\40_广度优先遍历邻接矩阵图.c"
# End Source File
# Begin Source File

SOURCE=".\41_广度优先遍历邻接表图.c"
# End Source File
# Begin Source File

SOURCE=".\42_插入排序算法.c"
# End Source File
# Begin Source File

SOURCE=".\43_冒泡排序算法.c"
# End Source File
# Begin Source File

SOURCE=".\44_快速排序算法.c"
# End Source File
# Begin Source File

SOURCE=".\45_直接选择排序.c"
# End Source File
# Begin Source File

SOURCE=".\46_二分查找法.c"
# End Source File
# Begin Source File

SOURCE=".\47_构建二叉排序树.c"
# End Source File
# Begin Source File

SOURCE=".\48_散列线性探测法查找.c"
# End Source File
# Begin Source File

SOURCE=".\二叉树.c"
# End Source File
# Begin Source File

SOURCE=".\链式存储队列.c"
# End Source File
# Begin Source File

SOURCE=".\排序元素.c"
# End Source File
# End Group
# Begin Group "Header Files"

# PROP Default_Filter "h;hpp;hxx;hm;inl"
# Begin Source File

SOURCE=".\二叉树.h"
# End Source File
# Begin Source File

SOURCE=".\链式存储队列.h"
# End Source File
# Begin Source File

SOURCE=".\排序元素.h"
# End Source File
# Begin Source File

SOURCE=".\顺序存储栈.h"
# End Source File
# Begin Source File

SOURCE=".\稀疏矩阵三元组.h"
# End Source File
# End Group
# Begin Group "Resource Files"

# PROP Default_Filter "ico;cur;bmp;dlg;rc2;rct;bin;rgs;gif;jpg;jpeg;jpe"
# End Group
# End Target
# End Project
