#include <stdio.h>
#include <stdlib.h>
#include "链式存储队列.h"

#define DATA_SIZE 5
// 定义图
typedef struct {
	// 定义顶点数组
	char vertexs[DATA_SIZE];
	// 顶点数量
	int vertexsCount;
	// 邻接矩阵
	int adjacencyMatrix[DATA_SIZE][DATA_SIZE];
	// 边数量
	int arcCount;
	
} Graph;

/*
	广度优先遍历无向图（邻接矩阵）

    v0 ———— v2
	|	      /	 | \
	|	    /	 |	\
	|	 /		 |	  v4
	|  /		 |	/
	|/			 | /
	v1 ———— v3

  邻接矩阵:
    0  1  1  0  0
	1  0  1  1  0
	1  1  0  1  1
	0  1  1  0  1
	0  0  1  1  0
 */
// @param startIndex 开始顶点下标
// @param n 顶点数量
int visited[100];
void breadthForeachGraph(int adjacencyMatrix[5][5], int startIndex, int n) {
	LinkedQueue* queue = initLinkedQueue_();
	int m,j;
	printf("v%d ", startIndex);
	visited[startIndex] = 1;
	offerLinkedQueue_(queue, startIndex + '0');

	while(!emptyLinkedQueue_(queue)) {
		m = popLinkedQueue_(queue) - '0';
		for(j = 0; j < n; j++) {
			if(adjacencyMatrix[m][j] == 1 && !visited[j]) {
				printf("v%d ", j);
				visited[j] = 1;
				offerLinkedQueue_(queue, j + '0');
			}
		}
	}
	
}

int main40() {
// 定义邻接矩阵
	int adjacencyMatrix[5][5] = {
		{0, 1, 1, 0, 0}, 
		{1, 0, 1, 1, 0}, 
		{1, 1, 0, 1, 1}, 
		{0, 1, 1, 0, 1}, 
		{0, 0, 1, 1, 0}
	};
	breadthForeachGraph(adjacencyMatrix, 0, 5);
	visited[0]=visited[1]=visited[2]=visited[3]=visited[4]=0;
	printf("\n");

	
	breadthForeachGraph(adjacencyMatrix, 1, 5);
	visited[0]=visited[1]=visited[2]=visited[3]=visited[4]=0;
	printf("\n");

	breadthForeachGraph(adjacencyMatrix, 2, 5);
	visited[0]=visited[1]=visited[2]=visited[3]=visited[4]=0;
	printf("\n");

	breadthForeachGraph(adjacencyMatrix, 3, 5);
	visited[0]=visited[1]=visited[2]=visited[3]=visited[4]=0;
	printf("\n");

	breadthForeachGraph(adjacencyMatrix, 4, 5);
	visited[0]=visited[1]=visited[2]=visited[3]=visited[4]=0;
	printf("\n");

	return 0;
}