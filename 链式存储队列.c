#include <stdio.h>
#include <stdlib.h>
#include "链式存储队列.h"


// 初始化queue 
LinkedQueue* initLinkedQueue_() {
	QueueNode *head = malloc(sizeof(QueueNode));
	LinkedQueue *queue = malloc(sizeof(LinkedQueue));
	queue->head = head;
	queue->rear = queue->head;
	queue->size = 0;
	return queue;
}

// 判断队列是否为空
int emptyLinkedQueue_(LinkedQueue *queue) {
	return queue->head == queue->rear;
}

// 入队
void offerLinkedQueue_(LinkedQueue *queue, DataType data) {
	QueueNode *dataNode = malloc(sizeof(QueueNode));
	dataNode->data = data;
	dataNode->next = NULL;
	queue->rear->next = dataNode;
	queue->rear = dataNode;
	queue->size++;
}

// 出队
DataType popLinkedQueue_(LinkedQueue *queue) {
	QueueNode *tmp = queue->head;
	if(emptyLinkedQueue_(queue)) {
		printf("empty queue!");
		exit(-1);
	}
	queue->head = queue->head->next;
	free(tmp);
	queue->size--;
	return queue->head->data;
}


void printLinkedQueue_(LinkedQueue *queue) {
	QueueNode *p = queue->head->next;
	if(emptyLinkedQueue_(queue)) {
		printf("empty queue!");
		return;
	}
	while(p != NULL) {
		printf("%c ", p->data);
		p = p->next;
	}
	printf(" -----> size: %d \n", queue->size);
}