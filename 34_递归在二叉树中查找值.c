#include <stdio.h>
#include <stdlib.h>
#include "二叉树.h"

/**
	二叉树中查找值为?的节点
 */
BTreeNode* searchDataBTree(BTree btree, DataType data) {
	BTreeNode *l_search = NULL, *r_search = NULL;
	if(btree == NULL) { 
		return NULL;
	}
	if(btree->data == data) {
		return btree;
	}
	l_search = searchDataBTree(btree->lchild, data);
	r_search = searchDataBTree(btree->rchild, data);
	return l_search != NULL ? l_search : r_search;
}

int main34() {
	/**
	  对应二叉树为
                A① 
             /     \
		    B②     C③
              \       \
			   E⑤     G⑦
              /
			 J⑩
	*/
		// [A] [B] [C] [@] [E] [@] [G] [@] [@] [J]            数组0任意填充
	DataType arr[] = {'@','A', 'B', 'C', '@', 'E', '@', 'G', '@', '@', 'J', '\0'};
	BTree btree = initBTreeByCompleteBTreeArr(arr);

	DataType c = 'E';
	BTreeNode *node = searchDataBTree(btree, c);
	if(node == NULL) {
		printf("not found %c\n", c);
	}else {
		printf("found %c!, it lchild: %c \n", c, node->lchild->data);
	}
	return 0;
}