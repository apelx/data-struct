#include <stdio.h>
#include <stdlib.h>
#define DATA_SIZE 5
// 定义图
typedef struct {
	// 定义顶点数组
	char vertexs[DATA_SIZE];
	// 顶点数量
	int vertexsCount;
	// 邻接矩阵
	int adjacencyMatrix[5][5];
	// 边数量
	int arcCount;
	
} Graph;

/*
	深度优先遍历无向图（邻接矩阵）

    v0 ———— v2
	|	      /	 | \
	|	    /	 |	\
	|	 /		 |	  v4
	|  /		 |	/
	|/			 | /
	v1 ———— v3

  邻接矩阵:
    0  1  1  0  0
	1  0  1  1  0
	1  1  0  1  1
	0  1  1  0  1
	0  0  1  1  0

 */
// @param vertextIndex 从第几个顶点(下标)遍历
// @param n 顶点数量
int visited[100];
void depthForeachGraph(int adjacencyMatrix[5][5],int vertextIndex, int n) {
	int j;
	printf("v%d ", vertextIndex);
	visited[vertextIndex] = 1;
	for(j = 0; j < n; j++) {
		if(adjacencyMatrix[vertextIndex][j] == 1 && !visited[j]) {
			depthForeachGraph(adjacencyMatrix, j, n);
		}
	}
}
int main38() {
	// 定义邻接矩阵
	int adjacencyMatrix[5][5] = {
		{0, 1, 1, 0, 0}, 
		{1, 0, 1, 1, 0}, 
		{1, 1, 0, 1, 1}, 
		{0, 1, 1, 0, 1}, 
		{0, 0, 1, 1, 0}
	};
	depthForeachGraph(adjacencyMatrix, 0, 5);
	visited[0]=visited[1]=visited[2]=visited[3]=visited[4]=0;
	printf("\n");

	depthForeachGraph(adjacencyMatrix, 1, 5);
	visited[0]=visited[1]=visited[2]=visited[3]=visited[4]=0;
	printf("\n");

	depthForeachGraph(adjacencyMatrix, 2, 5);
	visited[0]=visited[1]=visited[2]=visited[3]=visited[4]=0;
	printf("\n");

	depthForeachGraph(adjacencyMatrix, 3, 5);
	visited[0]=visited[1]=visited[2]=visited[3]=visited[4]=0;
	printf("\n");

	depthForeachGraph(adjacencyMatrix, 4, 5);
	visited[0]=visited[1]=visited[2]=visited[3]=visited[4]=0;
	printf("\n");

	return 0;
}