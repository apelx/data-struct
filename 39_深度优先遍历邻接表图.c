#include <stdio.h>
#include <stdlib.h>

#define MAX_SIZE 100

// 定义邻接表节点
typedef struct node {
	int index;
	struct node *next;
} Node;

// 定义邻接表条目
typedef struct {
	// 顶点名
	char vertext;
	Node *link;
} AdjacencyTableEntry;


// 定义邻接表
typedef struct {
	AdjacencyTableEntry *entries[MAX_SIZE];
	// 顶点数量
	int vertextCount;
	// 边数量
	int arcCount;
} AdjacencyTable;


/*
	深度优先遍历邻接表无向图

    v0 ———— v2
	|	      /	| \
	|	    /	|	\
	|	 /		|	  v4
	|  /		|	/
	|/			| /
	v1 ———— v3

  邻接表,链表存的是下标
下标
0		v0 []-> 1 -> 2
1		v1 []-> 0 -> 2 -> 3
2		v2 []-> 0 -> 1 -> 3 ->4
3		v3 []-> 1 -> 2 -> 4
4		v4 []-> 2 -> 3
 */
int visited[100];
// @param 从指定下标开始遍历
void depthForeachGraph1(AdjacencyTable *table, int startIndex) {
	Node *p;
	printf("v%d ", startIndex);
	visited[startIndex] = 1;
	p = table->entries[startIndex]->link;
	while(p != NULL) {
		if(!visited[p->index]) {
			depthForeachGraph1(table, p->index);
		}
		p = p->next;
	}
}


void printfAdjacencyTable1(AdjacencyTable *table) {
	int i;
	Node *node;
	for(i = 0; i < table->vertextCount; i++) {
		printf("vertext: v%c, arc node: \n", table->entries[i]->vertext);
		node = table->entries[i]->link;
		if(node == NULL) {
			printf("empty!\n");
		}else {
			while(node != NULL) {
				printf("v%d ", node->index);
				node = node->next;
			}
			printf("\n");
		}
	}
}

int main39() {
	int i;
	AdjacencyTable *table = malloc(sizeof(AdjacencyTable));
	table->arcCount = 7;
	table ->vertextCount = 5;

	for(i = 0; i < table->vertextCount; i++) {
		table->entries[i] = malloc(sizeof(AdjacencyTableEntry));
		table->entries[i]->vertext = i + '0';
		table->entries[i]->link = NULL;

	/*
0		v0 []-> 1 -> 2
1		v1 []-> 0 -> 2 -> 3
2		v2 []-> 0 -> 1 -> 3 ->4
3		v3 []-> 1 -> 2 -> 4
4		v4 []-> 2 -> 3
  */
		switch (i) {
			case 0:
				table->entries[i]->link = malloc(sizeof(Node));
				table->entries[i]->link->index = 1;
				table->entries[i]->link->next = malloc(sizeof(Node));
				table->entries[i]->link->next->index = 2;
				table->entries[i]->link->next->next = NULL;
				break;
			case 1:
				table->entries[i]->link = malloc(sizeof(Node));
				table->entries[i]->link->index = 0;

				table->entries[i]->link->next = malloc(sizeof(Node));
				table->entries[i]->link->next->index = 2;

				table->entries[i]->link->next->next = malloc(sizeof(Node));
				table->entries[i]->link->next->next->index = 3;
				table->entries[i]->link->next->next->next = NULL;
				break;
			case 2:
				table->entries[i]->link = malloc(sizeof(Node));
				table->entries[i]->link->index = 0;

				table->entries[i]->link->next = malloc(sizeof(Node));
				table->entries[i]->link->next->index = 1;

				table->entries[i]->link->next->next = malloc(sizeof(Node));
				table->entries[i]->link->next->next->index = 3;

				table->entries[i]->link->next->next->next = malloc(sizeof(Node));
				table->entries[i]->link->next->next->next->index = 4;
				table->entries[i]->link->next->next->next->next = NULL;
				break;
			case 3:
			table->entries[i]->link = malloc(sizeof(Node));
				table->entries[i]->link->index = 1;

				table->entries[i]->link->next = malloc(sizeof(Node));
				table->entries[i]->link->next->index = 2;

				table->entries[i]->link->next->next = malloc(sizeof(Node));
				table->entries[i]->link->next->next->index = 4;
				table->entries[i]->link->next->next->next = NULL;
				break;
			case 4:
				table->entries[i]->link = malloc(sizeof(Node));
				table->entries[i]->link->index = 2;
				table->entries[i]->link->next = malloc(sizeof(Node));
				table->entries[i]->link->next->index = 3;
				table->entries[i]->link->next->next = NULL;
				break;
		}
	}
	
	
	//printfAdjacencyTable1(table);

	depthForeachGraph1(table, 0);
	visited[0]=visited[1]=visited[2]=visited[3]=visited[4]=0;
	printf("\n");
	
	depthForeachGraph1(table, 1);
	visited[0]=visited[1]=visited[2]=visited[3]=visited[4]=0;
	printf("\n");

	depthForeachGraph1(table, 2);
	visited[0]=visited[1]=visited[2]=visited[3]=visited[4]=0;
	printf("\n");

	depthForeachGraph1(table, 3);
	visited[0]=visited[1]=visited[2]=visited[3]=visited[4]=0;
	printf("\n");

	depthForeachGraph1(table, 4);
	visited[0]=visited[1]=visited[2]=visited[3]=visited[4]=0;
	printf("\n");
	return 0;
}