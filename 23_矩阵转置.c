#include <stdio.h>
#include <stdlib.h>

#define line_size 3
#define column_size 6

/**
 * 将矩阵转置, 即原矩阵为 m * n，转置后为n * m 且 原[i][j] = 后[j][i] 0 < i <= m; 0 < j <= n;
 */
void trsmat(int arr1[line_size][column_size], int arr2[column_size][line_size]) {
	int i,j;
	for (i = 0; i < line_size; i++) {
		for (j = 0; j < column_size; j++) { 
			arr2[j][i] = arr1[i][j];
		}
	}
}

int main23() {
	
	int i,j;
	int arr1[line_size][column_size] = {
		{1,2,3,4,5,6}, 
		{7,8,9,10,11,12},
		{13,14,15,16,17,18}
	};
	int arr2[column_size][line_size] = {0};

 	trsmat(&arr1, &arr2);

	for(i = 0; i < column_size;  i++) {
		for (j = 0; j < line_size; j++) { 
			printf("%d ", arr2[i][j]);
		}
		printf("\n");
	}
	return 0;
}