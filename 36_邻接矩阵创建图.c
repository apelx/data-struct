#include <stdio.h>
#include <stdlib.h>

#define DATA_SIZE 100
typedef char DataType;

// 定义图
typedef struct {
	// 定义顶点数组
	DataType vertexs[DATA_SIZE];
	// 顶点数量
	int vertexsCount;
	// 邻接矩阵
	int adjacencyMatrix[DATA_SIZE][DATA_SIZE];
	// 边数量
	int arcCount;
	
} Graph;
	
// 构建带权值的邻接矩阵
Graph* buildWeightsGraph() {
	// 顶点数量、弧数量
	int vertexsCount, arcCount;
	Graph *graph = malloc(sizeof(Graph));
	int i, m, n, v;
	
	printf("please input how many vertexs has? \n");
	scanf("%d", &vertexsCount);
	printf("please input how many arc has? \n");
	scanf("%d", &arcCount);

	graph->vertexsCount = vertexsCount;
	graph->arcCount = arcCount;

	for(i = 0; i < vertexsCount; i++) {
		printf("please input number of [%d] vertex: \n", i + 1);
		// 注意！ 在C语言中，使用scanf("%c",a[i])连续输入字符型数据时，会把回车符也当做字符处理
		// 即输入字符a后，按下回车键，此时回车符被存在缓冲区，下一次输入字符b时，程序会先将缓冲区中的回车符存入数组
		// %c前面的空白符会使程序将缓存区的空白符吸收（或者将空白符换成\n或\t）
		scanf(" %c", &graph->vertexs[i]);
		
	}


	for(m = 0; m < vertexsCount; m++) {
		for(n = 0; n < vertexsCount; n++) {
			graph->adjacencyMatrix[m][n] = 65535;
		}
	}
	
	for(i = 0; i < arcCount; i++) {
		// 输入邻接矩阵的上三角或者下三角
		printf("please input numberOf [%d] arc's adjacencyMatrix i,j and Weights: \n", i + 1);
		scanf("%d %d %d", &m, &n, &v);
		printf("%d %d %d\n", m, n, v);
		graph->adjacencyMatrix[m][n] = v;
		graph->adjacencyMatrix[n][m] = v;
	}


	return graph;
}

void printWeightsGraph(Graph *graph) {
	int i, j;
	printf("vertexs: ");
	for (i = 0; i < graph->vertexsCount; i++) {
		printf("%c ", graph->vertexs[i]);
	}
	printf("\n");

	for (i = 0; i < graph->vertexsCount; i++) {
		for (j = 0; j < graph->vertexsCount; j++) {
			printf("%c->%c weights: %d\n", graph->vertexs[i], graph->vertexs[j], graph->adjacencyMatrix[i][j]);
		}
	}
}

int main36() {
	
	/**
		图(网络):
		v0 ——16— v1
		|\           |
		|  \21		 |
		19	 v4      6
		|  /    \14  |
		|/33	   \ |
		v2 ——18— v3

		对应邻接矩阵:
		∞  16  19  ∞  21
		16  ∞  ∞  6   ∞
		19  ∞  ∞  18  33
		∞  6   18  ∞  14
		21  ∞  33  14  ∞
	*/

	/*
		输入依次:
		5
		7
		a       -- 对应v0
		b		-- 对应v1
		c		-- 对应v2
		d		-- 对应v3
		e		-- 对应v4
		0 1 16
		0 2 19
		0 4 21
		1 3 6
		2 3 18
		2 4 33
		4 4 14
	*/
	Graph *graph = buildWeightsGraph();
	printWeightsGraph(graph);

	return 0;
}