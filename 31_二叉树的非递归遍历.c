#include <stdio.h>
#include <stdlib.h>
#include "二叉树.h"

void pushStackAndPrint(BTreeNodeStack *stack, BTreeNode *node) {
	pushBTreeNodeStack(stack, node);
	printf("%c ", node->data);
}

// 二叉树先序遍历 （非递归） DLR
void precedenceForeachBTreeNoRecursion(BTree btree) {
	BTreeNodeStack *stack = initBTreeNodeStack();
	BTreeNode *tempNode = btree;
	// 根节点入栈并打印
	pushStackAndPrint(stack, tempNode);

	while (!emptyBTreeNodeStack(stack)) {

		while(topBTreeNodeStack(stack)->lchild != NULL) {
			// 如果当前节点的右孩子为空, 当前节点出栈
			tempNode = topBTreeNodeStack(stack)->lchild;
			if(topBTreeNodeStack(stack)->rchild == NULL) {
				popBTreeNodeStack(stack);
			}
			pushStackAndPrint(stack, tempNode);
		}

		if(topBTreeNodeStack(stack)->rchild != NULL) {
			// 处理当前节点的右子树时，当前节点需要出栈
			pushStackAndPrint(stack, popBTreeNodeStack(stack)->rchild);
		} else {
			// 叶子节点出栈
			popBTreeNodeStack(stack);
			if(!emptyBTreeNodeStack(stack)) {
				// 栈顶的左子树全部处理完成, 出栈处理右子树
				pushStackAndPrint(stack, popBTreeNodeStack(stack)->rchild);
			}
		}
	}
	free(stack);
}

// 二叉树的中序遍历（非递归）
void middleForeachBTreeNoRecursion(BTree btree) {
	BTreeNodeStack *stack = initBTreeNodeStack();
	BTreeNode *tempNode = btree;
	pushBTreeNodeStack(stack, tempNode);
	while(!emptyBTreeNodeStack(stack)) {
		while(topBTreeNodeStack(stack)->lchild != NULL) {
			pushBTreeNodeStack(stack, topBTreeNodeStack(stack)->lchild);
		}
		tempNode = popBTreeNodeStack(stack);
		printf("%c ", tempNode->data);
		if(tempNode->rchild != NULL) {
			pushBTreeNodeStack(stack, tempNode->rchild);
		}else {
			tempNode = NULL;
			// 栈非空, 且栈顶的右孩子为空
			while(!emptyBTreeNodeStack(stack) && ((tempNode = popBTreeNodeStack(stack))->rchild == NULL)) {
				printf("%c ", tempNode->data);
			}
			if(tempNode != NULL) {
				printf("%c ", tempNode->data);
				pushBTreeNodeStack(stack, tempNode->rchild);
			}
		}
	}
	free(stack);
}

// 二叉树的后序遍历（非递归）
void postForeachBTreeNoRecursion(BTree btree) {
	BTreeNodeStack *stack = initBTreeNodeStack();
	BTreeNode *tempNode = btree;
	pushBTreeNodeStack(stack, tempNode);
	while(!emptyBTreeNodeStack(stack)) {
		while(topBTreeNodeStack(stack)->lchild != NULL) {
			pushBTreeNodeStack(stack, topBTreeNodeStack(stack)->lchild);
		}
		if(topBTreeNodeStack(stack)->rchild != NULL) {
			pushBTreeNodeStack(stack, topBTreeNodeStack(stack)->rchild);
		}else {
			// 栈非空
			while(!emptyBTreeNodeStack(stack)) {				
				// 出栈
				tempNode = popBTreeNodeStack(stack);
				printf("%c ", tempNode->data);
				// 栈非空 栈顶右孩子 != 刚刚出栈的元素
				if(!emptyBTreeNodeStack(stack) && topBTreeNodeStack(stack)->rchild != NULL && topBTreeNodeStack(stack)->rchild != tempNode) {
					pushBTreeNodeStack(stack, topBTreeNodeStack(stack)->rchild);
					break;
				}
			}
		}
	}
	free(stack);
}


int main31() {
	/**
	  对应二叉树为
                A① 
             /     \
		    B②     C③
              \       \
			   E⑤     G⑦
              /
			 J⑩
	*/
	

	// [A] [B] [C] [@] [E] [@] [G] [@] [@] [J]            数组0任意填充
	DataType arr[] = {'@','A', 'B', 'C', '@', 'E', '@', 'G', '@', '@', 'J', '\0'};
	BTree btree = initBTreeByCompleteBTreeArr(arr);
     
	// A B E J C G
	precedenceForeachBTreeNoRecursion(btree);
	printf("\n");
	// B J E A C G  
	middleForeachBTreeNoRecursion(btree);
	printf("\n");
	
	// J E B G C A
	postForeachBTreeNoRecursion(btree);
	printf("\n");
	
	return 0;
}