#include <stdio.h>
#include <stdlib.h>
#include "二叉树.h"


/////////////////////////////////////////////队列

// 初始化队列
BTreeNodeQueue *initBTreeNodeQueue() {
	BTreeNodeQueue *queue = malloc(sizeof(BTreeNodeQueue));
	queue->head = queue->rear = 0;
	return queue;
}

// 判断队列是否为空
int emptyBTreeNodeQueue(BTreeNodeQueue* queue){
	return queue->head == queue->rear;
}

// 判断队列是否满
int fullBTreeNodeQueue(BTreeNodeQueue* queue) {
	return (queue->rear + 1) % QUEUE_SIZE == queue->head;
}

// 入队  1成功 0失败
int enterBTreeNodeQueue(BTreeNodeQueue* queue, BTreeNode* node) {
	if(fullBTreeNodeQueue(queue)) {
		return 0;
	}
	queue->rear = (queue->rear + 1) % QUEUE_SIZE;
	queue->nodes[queue->rear] = node;
	return 1;
}

// 出队 非空弹出队首元素,否则返回NULL
BTreeNode* popBTreeNodeQueue(BTreeNodeQueue* queue){
	if(emptyBTreeNodeQueue(queue)) {
		return NULL;
	}
	queue->head = (queue->head + 1) % QUEUE_SIZE;
	return queue->nodes[queue->head];
}

// 获取队首元素  非空返回队首元素,否则返回NULL
BTreeNode* topBTreeNodeQueue(BTreeNodeQueue* queue) {
	if(emptyBTreeNodeQueue(queue)) {
		return NULL;
	}
	return queue->nodes[(queue->head + 1) % QUEUE_SIZE];
}

// 队列长度
int sizeofBTreeNodeQueue(BTreeNodeQueue *queue) {
	if(emptyBTreeNodeQueue(queue)) {
		return 0;
	}
	if(fullBTreeNodeQueue(queue)) {
		return QUEUE_SIZE - 1;
	}
	if(queue->rear > queue->head) {
		return queue->rear - queue->head;
	} else {
		// -1因为head不存数据   +1因为rear存数据
		// (QUEUE_SIZE - queue->head - 1) + (queue->rear + 1)
		return QUEUE_SIZE - queue->head + queue->rear;
	}
}

// 打印队列
void printBTreeNodeQueue(BTreeNodeQueue* queue) {
	int i;
	if(emptyBTreeNodeQueue(queue)) {
		printf("empty queue!\n");
	}
	i = (queue->head + 1) % QUEUE_SIZE;
	while(i != queue->rear) {
		printf("%c ", queue->nodes[i]->data);
		i++;
	}
	// 打印rear
	printf("%c \n", queue->nodes[i]->data);
}









///////////////////////////////////////////////栈

// 初始化空栈
BTreeNodeStack* initBTreeNodeStack() {
	BTreeNodeStack *stack = malloc(sizeof(BTreeNodeStack));
	stack->top = -1;
	return stack;
}

// 栈满
int fullBTreeNodeStack(BTreeNodeStack *stack) {
	return stack->top + 1 == STACK_SIZE;
}

// 栈空
int emptyBTreeNodeStack(BTreeNodeStack *stack) {
	return stack->top == -1;
}

// 压栈  1成功 0失败
int pushBTreeNodeStack(BTreeNodeStack *stack, BTreeNode *node) {
	if(fullBTreeNodeStack(stack)) {
		return 0;
	}
	stack->nodes[++stack->top] = node;
	return 1;
}

// 弹栈  栈非空弹出栈顶元素,否则返回NULL
BTreeNode* popBTreeNodeStack(BTreeNodeStack *stack) {
	if(emptyBTreeNodeStack(stack)) {
		return NULL;
	}
	return stack->nodes[stack->top--];
}

// 获取栈顶元素  栈非空返回栈顶元素,否则返回NULL
BTreeNode* topBTreeNodeStack(BTreeNodeStack *stack) {
	if(emptyBTreeNodeStack(stack)) {
		return NULL;
	}
	return stack->nodes[stack->top];
}

// 打印栈
void printBTreeNodeStack(BTreeNodeStack *stack) {
	int i;
	if(emptyBTreeNodeStack(stack)) {
		printf("empty stack!\n");
		return;
	}
	for(i = stack->top; i >= 0; i--) {
		printf("%c\n", stack->nodes[i]->data);
	}
	printf("\n");
}




// 先序遍历二叉树 DLR
void precedenceForeachBTree(BTreeNode *root) {
	printf("%c ", root->data);
	if(root->lchild != NULL) {
		precedenceForeachBTree(root->lchild);
	}
	if(root->rchild != NULL) {
		precedenceForeachBTree(root->rchild);
	}
}

// 中序遍历二叉树 LDR
void middleForeachBTree(BTreeNode *root) {
	if(root->lchild != NULL) {
		middleForeachBTree(root->lchild);
	}
	//printf("%c ", root->data);
	printf("%d ", root->data);
	if(root->rchild != NULL) {
		middleForeachBTree(root->rchild);
	}
}

// 后序遍历二叉树 LRD
void postForeachBTree(BTreeNode *root) {
	if(root->lchild != NULL) {
		postForeachBTree(root->lchild);
	}
	if(root->rchild != NULL) {
		postForeachBTree(root->rchild);
	}
	printf("%c ", root->data);
}


/**
	将完全二叉树的数组表现形式转换成 二叉链表（二叉树转换完全二叉树的数组，将不存在的节点补@在数组中）

	例如:
      [A] [B] [C] [@] [E] [@] [G] [@] [@] [J]
 下标  1   2   3   4   5   6   7   8   9  10             // 注意下标从1开始

  对应二叉树为
                A① 
             /     \
		    B②     C③
              \       \
			   E⑤     G⑦
              /
			 J⑩

*/
BTree initBTreeByCompleteBTreeArr(DataType arr[]) {
	BTree root = NULL;
	BTreeNode *tempNode;
	BTreeNode *nodes[100];
	int i = 1;
	while(arr[i] != '\0') {
		switch(arr[i]) {
			case '@':
				//printf("skip @：%d\n", i);
				break;
			default :
				tempNode = malloc(sizeof(BTreeNode));
				tempNode->data = arr[i];
				tempNode->lchild = tempNode->rchild = NULL;
				nodes[i] = tempNode;
				if(root == NULL) {
					root = tempNode;
				} else {
					// left child
					if(i % 2 == 0) {
						nodes[i/2]->lchild = tempNode;
					}
					// right child
					else {
						nodes[i/2]->rchild = tempNode;
					}
				}
				break;
		};
		i++;
	}
	return root;
}