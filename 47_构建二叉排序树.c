#include <stdio.h>
#include <stdlib.h>
#include "二叉树.h"

// 注意把二叉树.h 定义的DataType改成int -> typedef int DataType;

/**
 * 插入二叉排序树节点

  树左子树一定小于根, 右子树一定大于根
 */
BTree buildBSortTree(BTree tree, DataType insertVal) {
	BTreeNode *parent, *p = tree;
	BTreeNode *insertNode;
	if(tree == NULL) {
		tree = malloc(sizeof(BTree));
		tree->data = insertVal;
		tree->lchild = tree->rchild = NULL;
		return tree;
	}
	while(p != NULL) {
		parent = p;
		if(insertVal < p->data) {
			p = p->lchild;
		}else {
			p = p->rchild;
		}
	}
	insertNode = malloc(sizeof(BTreeNode));
	insertNode->lchild = insertNode->rchild = NULL;
	insertNode->data = insertVal;
	if(insertVal < parent->data) {
		parent->lchild = insertNode;
	}else {
		parent->rchild = insertNode;
	}
	return tree;
}

// 初始化二叉排序树
BTree initBSortTree(DataType arr[], int n) {
	int i;
	BTree tree = NULL;
	for(i = 0; i < n; i++) {
		tree = buildBSortTree(tree, arr[i]);
	}
	return tree;
}

// 在二叉排序树中查找指定值

BTreeNode *findBSortTreeVal(BTree tree, DataType val) {

	if(tree == NULL || val == tree->data) {
		return tree;
	}else if (val < tree->data) {
		return findBSortTreeVal(tree->lchild, val);
	}else  {
		return findBSortTreeVal(tree->rchild, val);
	}
}


int main47() {
	BTree tree;
	BTreeNode *node;
	DataType findVal;

	/*
				最终构建出的二叉排序树：

									20
								/		 \
							  10	      30
							/    \      /    \
						  5      15   25      35
					            /       \
							   12        27
	*/
	DataType arr[] = {20, 10, 30, 15, 25, 5, 35, 12, 27};
	tree = initBSortTree(arr, 9);

	// 中序遍历二叉树 即排序
	middleForeachBTree(tree);
	printf("\n");


	findVal = 30;
	node = findBSortTreeVal(tree, findVal);
	if(node == NULL) {
		printf("%d not found!\n", findVal);
	}else {
		printf("%d found! it val: %d, it lchild: %d, it rchild: %d\n", findVal, node->data, node->lchild->data, node->rchild->data);
	}
	return 0;
}