#include <stdio.h>

#define SORT_KEY int
#define MAX_SIZE 100

// 定义其他元素
typedef struct {
	char sex;
} OtherInfo;

// 定义排序元素
typedef struct {
	SORT_KEY key;
	// 其他元素, 代表一个实体, 例如学生成绩,学生等
	// OtherInfo otherInfo;
} SortTtem;

typedef SortTtem SortList[MAX_SIZE];

typedef SortTtem SearchList[MAX_SIZE];


// 打印排序集合
void printSortList(SortList, int, int);
