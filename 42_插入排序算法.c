#include <stdio.h>
#include <stdlib.h>
#include "排序元素.h"



// printSortList


/*
	插入排序:

	数组的0用来存临时变量
	从2个元素开始, 从后向前依次与当前元素之前的元素比较; 
	如果当前元素小于比较元素, 则将比较元素向后移动一位
	否则将当前元素插入比较位置！

         46  39  17  23  28  55  18  46
下标  0  1   2   3   4   5   6   7   8
*/
void insertSort(SortList sortList, int n) {
	int i,j;
	for(i = 2; i < n; i++) {
		sortList[0] = sortList[i];
		// 前一个元素
		j = i - 1;

		while(sortList[0].key < sortList[j].key) {
			sortList[j+1] = sortList[j];
			j--;
		}
		sortList[j+1] = sortList[0];
	}
}

int main42() {

	int n = 8;
	SortList sortList = {
		{0}, {46}, {39}, {17}, {23}, {28}, {55}, {18}, {46}
	};
	printSortList(sortList, 1, n);


	insertSort(sortList, n);

	printSortList(sortList, 1,  n);
	return 0;
}