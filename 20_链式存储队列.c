#include <stdio.h>
#include <stdlib.h>

typedef char DataType;

typedef struct QueueNode {
	DataType data;
	struct QueueNode *next;
}QueueNode;

typedef struct queue{
	struct QueueNode *head, *rear;
	int size;
} LinkedQueue;

void offerLinkedQueue(LinkedQueue*, DataType);

// 初始化queue 
LinkedQueue* initLinkedQueue() {
	QueueNode *head = malloc(sizeof(QueueNode));
	LinkedQueue *queue = malloc(sizeof(LinkedQueue));
	queue->head = head;
	queue->rear = queue->head;
	queue->size = 0;
	return queue;
}


// 初始化queue 通过数组
LinkedQueue* initLinkedQueueWithArr(DataType arr[]) {
	LinkedQueue *queue = initLinkedQueue();
	int i = 0;
	while(arr[i] != '\0') {
		offerLinkedQueue(queue, arr[i]);
		i++;
	}
	queue->size = i;
	return queue;
}

// 判断队列是否为空
int emptyLinkedQueue(LinkedQueue *queue) {
	return queue->head == queue->rear;
}

// 入队
void offerLinkedQueue(LinkedQueue *queue, DataType data) {
	QueueNode *dataNode = malloc(sizeof(QueueNode));
	dataNode->data = data;
	dataNode->next = NULL;
	queue->rear->next = dataNode;
	queue->rear = dataNode;
	queue->size++;
}

// 出队
// 出队是将头节点指向元素节点的第二个, 并且返回第一个; 但是如果队列中只有一个元素,需要考虑尾指针的指向
// 可以巧妙的将头节点删掉, 用第一个元素节点当作头节点, 这样就不用考虑尾指针; 但是头节点会有元素（不使用即可）
DataType popLinkedQueue(LinkedQueue *queue) {
	QueueNode *tmp = queue->head;
	if(emptyLinkedQueue(queue)) {
		printf("empty queue!");
		exit(-1);
	}
	queue->head = queue->head->next;
	free(tmp);
	queue->size--;
	return queue->head->data;
}


void printLinkedQueue(LinkedQueue *queue) {
	QueueNode *p = queue->head->next;
	if(emptyLinkedQueue(queue)) {
		printf("empty queue!");
		return;
	}
	while(p != NULL) {
		printf("%c ", p->data);
		p = p->next;
	}
	printf(" -----> size: %d \n", queue->size);
}


int main20() {
	DataType arr[] = {'a', 'b', 'c', 'd', 'e', '\0'};
	int i = 0;
	LinkedQueue *queue = initLinkedQueueWithArr(arr);
	printLinkedQueue(queue);

	for(; i < 2; i++) {
		printf("pop: %c \n", popLinkedQueue(queue));
		printLinkedQueue(queue);
	}
	

	printf("offer: 'f' \n");
	offerLinkedQueue(queue, 'f');
	printLinkedQueue(queue);

	printf("offer: 'g' \n");
	offerLinkedQueue(queue, 'g');
	printLinkedQueue(queue);


	for(i = 0; i < 5; i++) {
		printf("pop: %c \n", popLinkedQueue(queue));
		printLinkedQueue(queue);
	}
	return 0;
}