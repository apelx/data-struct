#include <stdio.h>
#include <stdlib.h>

#define LINE_SIZE 100
typedef int DataType;

/**
稀疏矩阵:
 0  3  1  0
 0  5  0  0
 0  0  0  0
 -4 0  0  -7

稀疏矩阵三元组:
 line    column    data
 0       1         3
 0       2         1                 数据行
 1       1         5
 3       0         -4
 3       3         -7
*/

// 三元组数据行
typedef struct {
	int line, column;
	DataType data;
} TriTupleLine;

// 稀疏矩阵三元组
typedef struct {
	// 三元组中所有行
	TriTupleLine lines[LINE_SIZE];
	// 稀疏矩阵的行数
	int line_count;
	// 稀疏矩阵的列数
	int column_count;
	// 非零元素的个数
	int no_zero_count;
} SparseMatrixTriTuple;