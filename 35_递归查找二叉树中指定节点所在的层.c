#include <stdio.h>
#include <stdlib.h>
#include "二叉树.h"

/**
	查找指定节点在二叉链中的层
	-1代表没找到
	@param last_c 上一层
 */
int seachDataLayerInBTree(BTree btree, DataType data, int last_layer) {
	int l_layer = -1, r_layer = -1;
	if(btree == NULL) {
		return -1;
	}
	if(btree->data == data) {
		return last_layer;
	}
	l_layer = seachDataLayerInBTree(btree->lchild, data, last_layer+1);
	r_layer = seachDataLayerInBTree(btree->rchild, data, last_layer+1);
	return l_layer != -1 ? l_layer : r_layer;
}

int main35() {
	/**
	  对应二叉树为
                A① 
             /     \
		    B②     C③
              \       \
			   E⑤     G⑦
              /
			 J⑩
	*/
		// [A] [B] [C] [@] [E] [@] [G] [@] [@] [J]            数组0任意填充
	DataType arr[] = {'@','A', 'B', 'C', '@', 'E', '@', 'G', '@', '@', 'J', '\0'};
	BTree btree = initBTreeByCompleteBTreeArr(arr);

	DataType c = 'B';
	int ceng = seachDataLayerInBTree(btree, c, 1);
	printf("%d\n", ceng);

	return 0;
}