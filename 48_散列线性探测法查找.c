#include <stdio.h>
#include <stdlib.h>

#define SIZE 11
// 定义散列表节点
typedef struct {
	int key;
	// 还有一些其他信息
	// ...
}TableNode;

// 定义散列表
typedef TableNode HashTable[SIZE];

/*
	散列存储时线性探测法：
	当散列的位置上有值，则 (key + i) % m, 直到散列位置上没有值

	已知序列（27， 13， 55， 32， 18， 49， 24， 38， 43） m=11,则散列表如下：

	0   1   2   3   4   5   6   7   8   9   10
	55  43  13  24      27  49  18  38      32

	设计一个算法，查找线性探测法散列存储
*/
int findHashTable(HashTable table, int key, int m) {
	int p = key % m;
	// 保存初始hash值
	int temp = p;
	int i = 1;
	if(table[p].key == key) {
		return p;
	}
	while(1) { 
		p = (key + i) % m;
		if(table[p].key == key) {
			return p;
		}
		 // 遇到空，代表没有这个值  // 如果一圈都没找到,则不存在
		if (table[p].key == -32768 || p == temp) {
			return -1;
		}

		i++;
	}
}

int main() {
	int findKey = 27;
	HashTable table = {{55}, {43}, {13}, {24}, {-32768}, {27}, {49}, {18}, {38}, {-32768}, {32}};

	
	printf("%d\n", findHashTable(table, findKey, SIZE));
	return 0;
}