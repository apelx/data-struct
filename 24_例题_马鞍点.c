#include <stdio.h>
#include <stdlib.h>

// m为行数 n为列数
#define m 3
#define n 4

/**
* 矩阵x中存在元素 x[i][j] 满足: x[i][]j]是第i行元素中最小值,并且又是第[j]列最大值,成为矩阵的一个马鞍点
* 求矩阵中所有马鞍点
*/
void ma_an_dian(int arr[m][n]) {
	// 分别存放每一行最小值、每一列最大值
	int line_min[m], column_max[n];
	int i,j;
	// 找到每一行最小值
	for(i = 0; i < m; i++) {
		line_min[i] = arr[i][0];
		for(j = 1; j < n; j++) {
			if(arr[i][j] < line_min[i]) {
				line_min[i] = arr[i][j];
			}
		}
	}

	// 找到每一列最小值
	for(i = 0; i < n; i++) {
		column_max[i] = arr[0][i];
		for(j = 1; j < m; j++) {
			if(arr[j][i] > column_max[i]) {
				column_max[i] = arr[j][i];
			}
		}
	}


	for(i = 0; i < m; i++) {
		for(j = 0; j < n; j++) {
			if(arr[i][j] == line_min[i] && arr[i][j] == column_max[j]) {
				printf("line: %d, column: %d, val: %d\n", i, j, arr[i][j]);
			}
		}
	}
}

int main24() {
	int arr[m][n] = {
		{7,   12,  3,  9},
		{11,  12,  8,  10},
		{4,   9,   2,  71}
	};

	ma_an_dian(arr);
	return 0;
}