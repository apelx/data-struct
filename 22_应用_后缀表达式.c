#include <stdio.h>
#include <stdlib.h>
#include "顺序存储栈.h"
#include "链式存储队列.h"

typedef char DataType;

int isNumber(DataType);
int isOperator(DataType);
int priority(DataType);
void processOperator(Stack*, LinkedQueue*, DataType);

/**
 * 中缀表达式转换后缀表达式
 * eg. 9-(2+4*7)/5+3    convertTo:  9247*+5/-3+
 *
 * 规则:
 * 1. 当读到数字时候,直接将其送至输出队列中
 * 2. 当读取到运算符时,将栈中所有优先级高于或等于该运算符的运算符弹出，送至输出队列，再将当前运算符入栈
 * 3. 当读取到左括号时，直接入栈
 * 4. 当读取到右括号时，将靠近栈顶的第一个左括号上面的运算符全部依次弹出，送至输出队列，再删除栈中的左括号
 */
LinkedQueue *convertSuffixExpression() {
	Stack *stack = initStack();
	LinkedQueue *queue = initLinkedQueue_();
	DataType data;

	scanf("%c", &data);
	while(data != '\n') {
		if(isNumber(data)) {
			offerLinkedQueue_(queue, data);
		}else if(isOperator(data)) {
			processOperator(stack, queue, data);
		}else {
			printf("invalid input");
			exit(-1);
		}
		//printf("stack ---> ");
		//printStack(stack);
		//printf("queue ---> ");
		//printLinkedQueue_(queue);
		scanf("%c", &data);
	}
	while(!emptyStack(stack)) {
		offerLinkedQueue_(queue, popStack(stack));
	}
	free(stack);
	return queue;
}

// 处理操作符
void processOperator(Stack *stack, LinkedQueue *queue, DataType data) {
	int dataPriority;
	switch(data) {
	case '+':
	case '-':
	case '*':
	case '/':
		dataPriority = priority(data);

		// 栈非空 && 栈顶元素!='(' && 栈顶元素优先级 >= 数据优先级
		while(!emptyStack(stack) && priority(getTopStack(stack)) >= dataPriority) {
			offerLinkedQueue_(queue, popStack(stack));
		}
		pushStack(stack, data);
		break;
	case '(':
		pushStack(stack, data);
		break;
	case ')':
		while(!emptyStack(stack) && getTopStack(stack) != '(') {
			offerLinkedQueue_(queue, popStack(stack));
		}
		if(!emptyStack(stack) && getTopStack(stack) == '(') {
			popStack(stack);
		}
		break;
	};
}

int isNumber(DataType data) {
	switch(data) {
	case '0':
	case '1':
	case '2':
	case '3':
	case '4':
	case '5':
	case '6':
	case '7':
	case '8':
	case '9':
		return 1;
	default:
		return 0;
	};
}

int isOperator(DataType data) {
	int res = 0;
	switch(data) {
	case '(':
	case ')':
	case '-':
	case '+':
	case '*':
	case '/':
		res = 1;
		break;
	};
	return res;
}

int isMathOperator(DataType data) {
	int res = 0;
	switch(data) {
	case '-':
	case '+':
	case '*':
	case '/':
		res = 1;
		break;
	};
	return res;
}


// 判断符号优先级
int priority(DataType data) {
	int priority = -1;
	switch(data) {
	case '(':
		priority = 0;
		break;
	case '+':
	case '-':
		priority = 1;
		break;
	case '*':
	case '/':
		priority = 2;
		break;
	default :
		priority = -1;
		break;
	};
	return priority;
}


//-------------------------------------------------------------------------

// 计算后缀表达式
int calcSuffixExpression(LinkedQueue *queue) {
	int res = -1,x,y;
	Stack *stack = initStack();
	DataType tmpData;
	while(!emptyLinkedQueue_(queue)) {
		tmpData = popLinkedQueue_(queue);
		if(isNumber(tmpData)) {
			// eg. '9'-'0' = 57-48 = 9, 但是stack里是字符, 所以存的是asic 9对应的字符
			pushStack(stack, tmpData - '0');
			printf("number! push stack char: [%c]; number: [%d]\n", tmpData - '0', tmpData - '0');
		}else if(isMathOperator(tmpData)) {
			y = popStack(stack);
			x = popStack(stack);
			switch(tmpData) {
			// 计算的时候将栈里的字符转成int 数据就对了，存的还是计算结果数字对应的asic字符
			case '+':
				printf("x=%d,y=%d, x+y. num: %d; char: %c\n", x, y, x + y, x + y);
				pushStack(stack, x + y);
				break;
			case '-':
				printf("x=%d,y=%d, x-y. num: %d; char: %c\n", x, y,  x - y, x - y);
				pushStack(stack, x - y);
				break;
			case '*':
				printf("x=%d,y=%d, x*y. num: %d; char: %c\n", x, y,  x * y, x * y);
				pushStack(stack, x * y);
				break;
			case '/':
				printf("x=%d,y=%d, x/y. num: %d; char: %c\n", x, y,  x / y, x / y);
				pushStack(stack, x / y);
				break;
			}
		}else {
			printf("invalid char");
			exit(-1);
		}
	}
	if(!emptyStack(stack)) {
		res = (int) popStack(stack);
	}
	free(stack);
	return res;

}

int main22() {
	
	
	LinkedQueue *queue = convertSuffixExpression();
	printLinkedQueue_(queue);

	printf("calc result: %d\n", calcSuffixExpression(queue));

  /*
	DataType x,y, ch;
	int a,b,res;

		Stack *stack = initStack();

		pushStack(stack, '4' - '0');
		pushStack(stack, '7' - '0');

	
		pushStack(stack, ((int)popStack(stack)) * ((int)popStack(stack)));
		
		printf("%d", getTopStack(stack));
		*/
	return 0;
}