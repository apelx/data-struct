#include <stdio.h>
#include <stdlib.h>

#define STACK_SIZE 100
#define QUEUE_SIZE 100
//typedef char DataType;
typedef int DataType;


// 二叉树节点
typedef struct bt_node {
	DataType data;
	struct bt_node *lchild,*rchild;
} BTreeNode;

typedef BTreeNode* BTree;


// 定义二叉树节点栈
typedef struct {
	BTreeNode *nodes[STACK_SIZE];
	int top;
}BTreeNodeStack;

// 定义二叉树节点队列
typedef struct {
	BTreeNode *nodes[QUEUE_SIZE];
	int rear,head;
}BTreeNodeQueue;


// 初始化队列
BTreeNodeQueue *initBTreeNodeQueue();
// 判断队列是否为空
int emptyBTreeNodeQueue(BTreeNodeQueue*);
// 判断队列是否满
int fullBTreeNodeQueue(BTreeNodeQueue*);
// 入队  1成功 0失败
int enterBTreeNodeQueue(BTreeNodeQueue*, BTreeNode*);
// 出队 非空弹出队首元素,否则返回NULL
BTreeNode *popBTreeNodeQueue(BTreeNodeQueue*);
// 获取队首元素  非空返回队首元素,否则返回NULL
BTreeNode* topBTreeNodeQueue(BTreeNodeQueue*);
// 队列长度
int sizeofBTreeNodeQueue(BTreeNodeQueue *);
// 打印队列
void printBTreeNodeQueue(BTreeNodeQueue*);


// 初始化空栈
BTreeNodeStack* initBTreeNodeStack();
// 栈满
int fullBTreeNodeStack(BTreeNodeStack*);
// 栈空
int emptyBTreeNodeStack(BTreeNodeStack*);
// 压栈  1成功 0失败
int pushBTreeNodeStack(BTreeNodeStack*, BTreeNode*);
// 弹栈  栈非空弹出栈顶元素,否则返回NULL
BTreeNode* popBTreeNodeStack(BTreeNodeStack*);
// 获取栈顶元素  栈非空返回栈顶元素,否则返回NULL
BTreeNode* topBTreeNodeStack(BTreeNodeStack*);
// 打印栈
void printBTreeNodeStack(BTreeNodeStack*);
// 先序遍历二叉树 DLR
void precedenceForeachBTree(BTreeNode*);
// 中序遍历二叉树 LDR
void middleForeachBTree(BTreeNode*);
// 后序遍历二叉树 LRD
void postForeachBTree(BTreeNode*);


// 将完全二叉树的数组表现形式转换成 二叉链表
BTree initBTreeByCompleteBTreeArr(DataType[]);